#include "mbed.h"
#include "hcsr04.h"


Serial pc(USBTX, USBRX);
HCSR04 dsensor(D7,D6);


void init(void){
  pc.baud(115200);
  pc.printf("--Ultrasonic telemeter--\n");
  pc.printf("--Test Begin--\n");
}

void loop (void){
  dsensor.run();
  pc.printf("Distance measured: %f \r\n", dsensor.getDistance());
  wait_ms(100);
}

int main(void){
  init();
  while(1){
    loop();
  }
}
