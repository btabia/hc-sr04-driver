#include "pwmin.h"

PwmIn::PwmIn(PinName _pin) : _p(_pin){
  _p.rise(this, &PwmIn::rise);
  _p.fall(this, &PwmIn::fall);
  _period = 0.0;
  _pulsewidth = 0.0;
  _t.start();
}

float PwmIn::period(){
  return _period;
}

float PwmIn::pulseWidth(){
  return _pulsewidth;
}

float PwmIn::dutycycle(){
  return _pulsewidth / _period;
}

void PwmIn::rise(){
  _period = _t.read();
  _t.reset();
}

void PwmIn::fall(){
  _pulsewidth = _t.read();
}
