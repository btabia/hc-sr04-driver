
// Ultrasonic ranging module HC - SR04 provides 2cm - 400cm non-contact measurement function, the ranging accuracy can reach to 3mm.
// The modules includes ultrasonic transmitters, receiver and control circuit. The basic principle of work:
// (1) Using IO trigger for at least 10us high level signal,
// (2) The Module automatically sends eight 40 kHz and detect whether there is a pulse signal back.
// (3) IF the signal back, through high level , time of high output IO duration is the time from sending ultrasonic to returning.
// Test distance = (high level time×velocity of sound (340M/S) / 2,
// Wire connecting direct as following:
// 􏰀 5V Supply
// 􏰀 Trigger Pulse Input 􏰀 Echo Pulse Output 􏰀 0V Ground
#ifndef HCSR04_H
#define HCSR04_H

#include "mbed.h"
#include "pwmin.h"

class HCSR04{
public:
  HCSR04(PinName _Pin, PinName _Pout);


  void run(void);
  float getDistance(void) {return distance;}
  float getEcho(void) {return echoTime;}

private:

  void trigger(void);
  void measureEcho(void);
  void calculateDistance(void);

  float distance;
  float echoTime;

  DigitalOut triggerSignal;
  PwmIn echoSignal;

};

#endif
