#include "hcsr04.h"


HCSR04::HCSR04(PinName _Pin, PinName _Pout):
triggerSignal(_Pin),
echoSignal(_Pout)
{
  triggerSignal.write(false);
  distance = 0;
  echoTime = 0;
}

void HCSR04::run(){
  trigger();
  measureEcho();
  calculateDistance();
}

void HCSR04::trigger(){
  triggerSignal.write(true);
  wait_us(10);
  triggerSignal.write(false);
}

void HCSR04::measureEcho(){
  echoTime = echoSignal.pulseWidth();
}

void HCSR04::calculateDistance(){
  distance = echoTime * 34000 / 2;
}
